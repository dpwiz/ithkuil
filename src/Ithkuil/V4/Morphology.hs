module Ithkuil.V4.Morphology
  ( Formative
  , Adjunct
  ) where

import Ithkuil.V4.Morphology.Formative (Formative)
import Ithkuil.V4.Morphology.Adjunct (Adjunct)
