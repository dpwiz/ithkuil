module Ithkuil.V4.Morphology.Formative
  ( Formative (..),

    Vv (..),
    Cr (..),
    Vr (..),
    CsVx (..),
    Ca (..),
    VxCs (..),
    VnVt1 (..),
    VpVlVeVt2Vm (..),
    CcCm (..),
    VcVk (..),
    Cb (..),

    Cd (..),
    Vf (..),
    Ci (..),
    Stress (..),
  )
where

import Ithkuil.V4.Categories

data Formative
  = Simple
      { _slot4  :: Vv,
        _slot5  :: Cr,
        _slot6  :: Vr,
        _slot7  :: [CsVx],
        _slot8  :: Ca,
        _slot9  :: [VxCs],
        _slot10 :: (Maybe VnVt1),
        _slot11 :: (Maybe VpVlVeVt2Vm),
        _slot12 :: (Maybe CcCm),
        _slot13 :: Maybe VcVk,
        _slot14 :: (Maybe Cb),
        _slot15 :: Stress
      }
  | Complex
      { _slot1  :: Cd,
        _slot2  :: Vf,
        _slot3  :: Ci,
        _slot4  :: Vv,
        _slot5  :: Cr,
        _slot6  :: Vr,
        _slot7  :: [CsVx],
        _slot8  :: Ca,
        _slot9  :: [VxCs],
        _slot10 :: (Maybe VnVt1),
        _slot11 :: (Maybe VpVlVeVt2Vm),
        _slot12 :: (Maybe CcCm),
        _slot13 :: Maybe VcVk,
        _slot14 :: (Maybe Cb),
        _slot15 :: Stress
      }
  | Short
      { _slot5    :: Cr,
        _slot6    :: Vr,
        _shortFML :: Bool,
        _shortCPT :: Bool,
        _slot7    :: [CsVx],
        _slot8    :: Ca,
        _slot9    :: [VxCs],
        _slot10   :: (Maybe VnVt1),
        _slot11   :: (Maybe VpVlVeVt2Vm),
        _slot12   :: (Maybe CcCm),
        _slot13   :: Maybe VcVk,
        _slot14   :: (Maybe Cb),
        _slot15   :: Stress
      }
  deriving (Eq, Ord, Show)

-- | Slot 1
--
-- Consonant form beginning with h- w- y- or ř-.
data Cd = Cd
  { _incDesignation :: Designation,
    _incVersion     :: Version,
    _incType        :: IncType
  } deriving (Eq, Ord, Show)

-- | Slot 2
--
-- Format of incorporated root.
--
-- Same forms used for Slot 13 Vc
data Vf = Vf Case
  deriving (Eq, Ord, Show)

-- | Slot 3
data Ci = Ci
  { _incRoot :: Root
  } deriving (Eq, Ord, Show)

-- | Slot 4
--
-- Glottal stop pronounced but not written.
data Vv = Vv
  { _designation :: Designation,
    _version     :: Version,
    _distinction :: Distinction
  } deriving (Eq, Ord, Show)

-- | Slot 5
data Cr = Cr
  { _mainRoot :: Root
  }
  deriving (Eq, Ord, Show)

-- | Slot 6
data Vr = Vr
  { _specification :: Specification,
    _stem          :: Stem,
    _function      :: Function
  } deriving (Eq, Ord, Show)

-- | Slot 7
--
-- Stem affixes.
--
-- The last Vx in the series requires a glottal stop to indicate end of slot.
data CsVx = CsVx
  { _stemAffix :: Affix
  } deriving (Eq, Ord, Show)

-- | Slot 8
--
-- Consonantal form
data Ca = Ca
  { _configuration :: Configuration,
    _affiliation   :: Affiliation,
    _extension     :: Extension,
    _perspective   :: Perspective,
    _essence       :: Essence
  } deriving (Eq, Ord, Show)

-- | Slot 9
--
-- Apply to main stem + Ca
data VxCs = VxCs
  { _stemCaAffix :: Affix
  } deriving (Eq, Ord, Show)

-- | Slot 10
--
-- Modular slot.
--
-- Aspect or Mood forms appear only if Vn Valence = MNO
data VnVt1
  = Vn  Valence Context
  | Vt1 Aspect
  deriving (Eq, Ord, Show)

-- | Slot 11
--
-- Modular slot.
data VpVlVeVt2Vm
  = Vp  Phase
  | Vl  Level
  | Ve  Effect
  | Vt2 Aspect
  | Vm  Mood
  deriving (Eq, Ord, Show)

-- | Slot 12
--
-- Consonantal affix.
data CcCm
  = CaseScope     CaseScope
  | Mood          Mood
  | CaseScopeMood CaseScope Mood
  deriving (Eq, Ord, Show)

-- | Slot 13
--
-- Determined by Distinction in Slot 4.
--
-- Vocalic affix.
data VcVk
  = Vc Case
  | Vk Illocution Sanction
  deriving (Eq, Ord, Show)

-- | Slot 14
--
-- Preceded by a glottal stop unless Slot 11 is filled or
-- the Slot 13 affix contains a -w- or -y-.
data Cb = Cb
  { _bias :: Bias
  } deriving (Eq, Ord, Show)

-- | Slot 15
data Stress
  = Stress
      { _stress :: Bool,
        _shift  :: Bool
      }
  deriving (Eq, Ord, Show)
