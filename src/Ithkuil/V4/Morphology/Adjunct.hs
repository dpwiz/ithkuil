module Ithkuil.V4.Morphology.Adjunct
  ( Adjunct (..),

    CmCc (..),
    VnVt1Vm1 (..),
    VpVlVeVt2Vm2 (..),
    Stress (..),
  )
where

import Ithkuil.V4.Categories

data Adjunct
  = Modular
      { m_slot1 :: Maybe CmCc,
        m_slot2 :: Maybe VnVt1Vm1,
        m_slot3 :: Maybe VpVlVeVt2Vm2,
        m_slot4 :: Maybe VpVlVeVt2Vm2,
        m_slot5 :: Maybe Stress
      }
  | SingleAffixual
      { sa_slot1 :: Bool,
        sa_slot2 :: VxCs
      }
  | AffixualScoping
      { as_slot1 :: Bool,
        as_slot2 :: [VxCs],
        as_slot3 :: Maybe AffixScope2,
        as_slot4 :: Maybe AffixScope5,
        as_slot5 :: [VxCs],
        as_slot6 :: Stress
      }
  | Carrier
      { c_slot1 :: Vc
      }
  | Concatenative
      { ca_slot1 :: Vc
      }
  | Register
      { r_slot1 :: Register
      }
  | PersonalReferent
      { pr_slot1 :: Maybe Vc,
        pr_slot2 :: Maybe Cpr,
        pr_slot3 :: Maybe EpE',
        pr_slot4 :: Cpr,
        pr_slot5 :: Vc
      }
  | PersonalReferentCombo
      { prc_slot1 :: Vv,
        prc_slot2 :: (Cpr, Cpr, Maybe Cpr),
        prc_slot3 :: Vc,
        prc_slot4 :: Specification,
        prc_slot5 :: [VxCs],
        prc_slot6 :: Vc2VkEp,
        prc_slot7 :: PrcStress
      }
  | Parsing
      { p_slot1 :: Parsing
      }
  deriving (Eq, Ord, Show)

data CmCc
  = Cm Mood
  | Cc CaseScope
  deriving (Eq, Ord, Show)

data VnVt1Vm1
  = Vn Valence
  | Vt1 Aspect
  | Vm1 Mood
  deriving (Eq, Ord, Show)

data VpVlVeVt2Vm2
  = Vp Phase
  | Vl Level
  | Ve Effect
  | Vt2 Aspect
  | Vm2 Mood
  deriving (Eq, Ord, Show)

data Stress
  = ByFormative
  | Nominal
  deriving (Eq, Ord, Show)

data VxCs
  = VxCs
      { _stemAffix :: Affix
      }
  deriving (Eq, Ord, Show)

data AffixScope2
  = As2A
  | As2E
  | As2O
  | As2I
  deriving (Eq, Ord, Show)

data AffixScope5
  = As5wy
  | As5h
  | As5'wy
  | As5'h
  deriving (Eq, Ord, Show)

data Vc
  = Vc Case
  deriving (Eq, Ord, Show)

data Cpr
  = Cpr
      { _referent :: PrParty,
        _effect   :: PrEffect
      }
  deriving (Eq, Ord, Show)

data EpE'
  = EpE'
  deriving (Eq, Ord, Show)

data Vv
  = Vv
      { _specification :: Specification,
        _stem          :: Stem,
        _function      :: Function
      }
  deriving (Eq, Ord, Show)

data Vc2VkEp
  = Vc2 Case
  | Vk Illocution Sanction
  | EpA
  deriving (Eq, Ord, Show)

data PrcStress
  = PrcPenultimate
  | PrcUltimate
  deriving (Eq, Ord, Show)
