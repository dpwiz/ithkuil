module Ithkuil.V4.Categories
  ( Affiliation,
    Affix,
    Aspect,
    Bias,
    Case,
    CaseScope,
    Configuration,
    Context,
    Designation,
    Distinction,
    Effect,
    Essence,
    Extension,
    Function,
    Illocution,
    IncType,
    Level,
    Mood,
    Parsing,
    Perspective,
    Phase,
    PrEffect,
    PrParty,
    Register,
    Root,
    Sanction,
    Specification,
    Stem,
    Valence,
    Version,
  )
where

import Ithkuil.V4.Categories.Affiliation (Affiliation)
import Ithkuil.V4.Categories.Affix (Affix)
import Ithkuil.V4.Categories.Aspect (Aspect)
import Ithkuil.V4.Categories.Bias (Bias)
import Ithkuil.V4.Categories.Case (Case)
import Ithkuil.V4.Categories.CaseScope (CaseScope)
import Ithkuil.V4.Categories.Configuration (Configuration)
import Ithkuil.V4.Categories.Context (Context)
import Ithkuil.V4.Categories.Designation (Designation)
import Ithkuil.V4.Categories.Distinction (Distinction)
import Ithkuil.V4.Categories.Effect (Effect)
import Ithkuil.V4.Categories.Essence (Essence)
import Ithkuil.V4.Categories.Extension (Extension)
import Ithkuil.V4.Categories.Function (Function)
import Ithkuil.V4.Categories.Illocution (Illocution)
import Ithkuil.V4.Categories.IncType (IncType)
import Ithkuil.V4.Categories.Level (Level)
import Ithkuil.V4.Categories.Mood (Mood)
import Ithkuil.V4.Categories.Parsing (Parsing)
import Ithkuil.V4.Categories.Perspective (Perspective)
import Ithkuil.V4.Categories.Phase (Phase)
import Ithkuil.V4.Categories.PrParty (PrParty)
import Ithkuil.V4.Categories.PrEffect (PrEffect)
import Ithkuil.V4.Categories.Register (Register)
import Ithkuil.V4.Categories.Root (Root)
import Ithkuil.V4.Categories.Sanction (Sanction)
import Ithkuil.V4.Categories.Specification (Specification)
import Ithkuil.V4.Categories.Stem (Stem)
import Ithkuil.V4.Categories.Valence (Valence)
import Ithkuil.V4.Categories.Version (Version)
