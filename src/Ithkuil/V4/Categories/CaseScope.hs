module Ithkuil.V4.Categories.CaseScope
  ( CaseScope (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data CaseScope
  = -- | X’s is governed by the noun-case of the formative
    -- marked CC = -hl-; in the absence of such, X’s case
    -- is associated with the main verb (or framed verb
    -- if within a case-frame).
    CCh
  | -- | X is the “head” whose case governs all CC-unmarked
    -- nouns in the clause (or nouns marked with CC = -h-/-ç-).
    CCl
  | -- | X is the formative to which formatives in the clause
    -- marked with CC = -çç- are associated.
    CCr
  | -- | X is associated by noun-case to the formative
    -- marked by CC = -hr.
    CCw
  | -- | X’s noun-case associates only with the immediately
    -- following formative.
    CCm
  | -- | X’s noun-case associates only with the immediately
    -- preceding formative.
    CCn
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated CaseScope where
  toAbbr = Abbr . \case
    CCh -> "CCh"
    CCl -> "CCl"
    CCr -> "CCr"
    CCw -> "CCw"
    CCm -> "CCm"
    CCn -> "CCn"

  fromAbbr (Abbr a) = case a of
    "CCh" -> Just CCh
    "CCl" -> Just CCl
    "CCr" -> Just CCr
    "CCw" -> Just CCw
    "CCm" -> Just CCm
    "CCn" -> Just CCn
    _     -> Nothing
