module Ithkuil.V4.Categories.Illocution
  ( Illocution (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Illocution
  = CNF
  | INF
  | ITU
  | REV
  | HSY
  | USP
  | DIR
  | IRG
  | DEC
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Illocution where
  toAbbr = Abbr . \case
    CNF -> "CNF"
    INF -> "INF"
    ITU -> "ITU"
    REV -> "REV"
    HSY -> "HSY"
    USP -> "USP"
    DIR -> "DIR"
    IRG -> "IRG"
    DEC -> "DEC"

  fromAbbr (Abbr a) = case a of
    "CNF" -> Just CNF
    "INF" -> Just INF
    "ITU" -> Just ITU
    "REV" -> Just REV
    "HSY" -> Just HSY
    "USP" -> Just USP
    "DIR" -> Just DIR
    "IRG" -> Just IRG
    "DEC" -> Just DEC
    _     -> Nothing
