module Ithkuil.V4.Categories.Effect
  ( Effect (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Effect
  = Ben1
  | Ben2
  | Ben3
  | BenAll
  | Unk
  | DetAll
  | Det3
  | Det2
  | Det1
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Effect where
  toAbbr = Abbr . \case
    Ben1    -> "1/BEN"
    Ben2    -> "2/BEN"
    Ben3    -> "3/BEN"
    BenAll  -> "all/BEN"
    Unk     -> "UNK"
    DetAll  -> "all/DET"
    Det3    -> "3/DET"
    Det2    -> "2/DET"
    Det1    -> "1/DET"

  fromAbbr (Abbr a) = case a of
    "1/BEN"   -> Just Ben1
    "2/BEN"   -> Just Ben2
    "3/BEN"   -> Just Ben3
    "all/BEN" -> Just BenAll
    "UNK"     -> Just Unk
    "all/DET" -> Just DetAll
    "3/DET"   -> Just Det3
    "2/DET"   -> Just Det2
    "1/DET"   -> Just Det1
    _         -> Nothing
