module Ithkuil.V4.Categories.PrEffect
  ( PrEffect(..),
  )
where

data PrEffect
  = Neutral
  | Beneficial
  | Detrimental
  deriving (Eq, Ord, Enum, Bounded, Show)
