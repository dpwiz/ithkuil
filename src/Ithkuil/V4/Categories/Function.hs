module Ithkuil.V4.Categories.Function
  ( Function(..),
  )
where

import Language.Class.Abbr (Abbreviated(..), Abbr(..))

data Function
  = Static
  | Dynamic
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Function where
  toAbbr = Abbr . \case
    Static  -> "STA"
    Dynamic -> "DYN"

  fromAbbr (Abbr a) = case a of
    "STA" -> Just Static
    "DYN" -> Just Dynamic
    _     -> Nothing
