module Ithkuil.V4.Categories.PrParty
  ( PrParty(..),
  )
where

import Language.Class.Abbr (Abbreviated(..), Abbr(..))

data PrParty
  = MonadicSpeaker
  | MonadicAddressee
  | PolyadicAddressee
  | MonadicAnimate3
  | PolyadicAnimate3
  | MonadicInanimate3
  | PolyadicInanimate3
  | MixedAI3
  | Obviative
  | ImpersonalAnimate
  | ImpersonalInanimate
  | NomicAI
  | AbstractAI
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated PrParty where
  toAbbr = Abbr . \case
    MonadicSpeaker      -> "1m"
    MonadicAddressee    -> "2m"
    PolyadicAddressee   -> "2p"
    MonadicAnimate3     -> "ma"
    PolyadicAnimate3    -> "pa"
    MonadicInanimate3   -> "mi"
    PolyadicInanimate3  -> "pi"
    MixedAI3            -> "Mx"
    Obviative           -> "Obc"
    ImpersonalAnimate   -> "IPa"
    ImpersonalInanimate -> "IPi"
    NomicAI             -> "Nai"
    AbstractAI          -> "Aai"

  fromAbbr (Abbr a) = case a of
    "1m"  -> Just MonadicSpeaker
    "2m"  -> Just MonadicAddressee
    "2p"  -> Just PolyadicAddressee
    "ma"  -> Just MonadicAnimate3
    "pa"  -> Just PolyadicAnimate3
    "mi"  -> Just MonadicInanimate3
    "pi"  -> Just PolyadicInanimate3
    "Mx"  -> Just MixedAI3
    "Obc" -> Just Obviative
    "IPa" -> Just ImpersonalAnimate
    "IPi" -> Just ImpersonalInanimate
    "Nai" -> Just NomicAI
    "Aai" -> Just AbstractAI
    _     -> Nothing
