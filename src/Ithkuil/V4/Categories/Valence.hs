module Ithkuil.V4.Categories.Valence
  ( Valence (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Valence
  = Monoactive
  | Parallel
  | Corollary
  | Reciprocal
  | Complementary
  | Duplicative
  | Demonstrative
  | Contingent
  | Participative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Valence where
  toAbbr = Abbr . \case
    Monoactive    -> "MNO"
    Parallel      -> "PRL"
    Corollary     -> "CRO"
    Reciprocal    -> "RCP"
    Complementary -> "CPL"
    Duplicative   -> "DUP"
    Demonstrative -> "DEM"
    Contingent    -> "CNG"
    Participative -> "PTI"

  fromAbbr (Abbr a) = case a of
    "MNO" -> Just Monoactive
    "PRL" -> Just Parallel
    "CRO" -> Just Corollary
    "RCP" -> Just Reciprocal
    "CPL" -> Just Complementary
    "DUP" -> Just Duplicative
    "DEM" -> Just Demonstrative
    "CNG" -> Just Contingent
    "PTI" -> Just Participative
    _     -> Nothing
