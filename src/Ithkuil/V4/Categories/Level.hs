module Ithkuil.V4.Categories.Level
  ( Level (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Level
  = Minimal
  | Equative
  | Surpassive
  | Deficient
  | Maximal
  | Superlative
  | Inferior
  | Superequative
  | Subequative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Level where
  toAbbr = Abbr . \case
    Minimal       -> "MIN"
    Subequative   -> "SBE"
    Inferior      -> "IFR"
    Deficient     -> "DEF"
    Equative      -> "EQU"
    Surpassive    -> "SUR"
    Superlative   -> "SPL"
    Superequative -> "SPQ"
    Maximal       -> "MAX"

  fromAbbr (Abbr a) = case a of
    "MIN" -> Just Minimal
    "SBE" -> Just Subequative
    "IFR" -> Just Inferior
    "DEF" -> Just Deficient
    "EQU" -> Just Equative
    "SUR" -> Just Surpassive
    "SPL" -> Just Superlative
    "SPQ" -> Just Superequative
    "MAX" -> Just Maximal
    _     -> Nothing
