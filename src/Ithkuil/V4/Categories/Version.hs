module Ithkuil.V4.Categories.Version
  ( Version(..)
  ) where

data Version
  = Processual
  | Completive
  deriving (Eq, Ord, Enum, Bounded, Show)
