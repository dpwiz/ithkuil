module Ithkuil.V4.Categories.Extension
  ( Extension (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Extension
  = Delimitive
  | Proximal
  | Incipient
  | Attenuative
  | Graduative
  | Depletive
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Extension where
  toAbbr = Abbr . \case
    Delimitive  -> "DEL"
    Proximal    -> "PRX"
    Incipient   -> "ICP"
    Attenuative -> "ATV"
    Graduative  -> "GRA"
    Depletive   -> "DPL"

  fromAbbr (Abbr a) = case a of
    "DEL" -> Just Delimitive
    "PRX" -> Just Proximal
    "ICP" -> Just Incipient
    "ATV" -> Just Attenuative
    "GRA" -> Just Graduative
    "DPL" -> Just Depletive
    _     -> Nothing
