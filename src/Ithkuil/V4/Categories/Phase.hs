module Ithkuil.V4.Categories.Phase
  ( Phase (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Phase
  = Contextual
  | Punctual
  | Iterative
  | Repetitive
  | Intermittent
  | Recurrent
  | Frequentative
  | Fragmentative
  | Fluctuative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Phase where
  toAbbr = Abbr . \case
    Contextual    -> "CTX"
    Punctual      -> "PCT"
    Iterative     -> "ITR"
    Repetitive    -> "REP"
    Intermittent  -> "ITM"
    Recurrent     -> "RCT"
    Frequentative -> "FRE"
    Fragmentative -> "FRG"
    Fluctuative   -> "FLC"

  fromAbbr (Abbr a) = case a of
    "CTX" -> Just Contextual
    "PCT" -> Just Punctual
    "ITR" -> Just Iterative
    "REP" -> Just Repetitive
    "ITM" -> Just Intermittent
    "RCT" -> Just Recurrent
    "FRE" -> Just Frequentative
    "FRG" -> Just Fragmentative
    "FLC" -> Just Fluctuative
    _     -> Nothing
