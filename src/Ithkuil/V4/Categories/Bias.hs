module Ithkuil.V4.Categories.Bias
  ( Bias(..),
  )
where

import Language.Class.Abbr (Abbreviated(..), Abbr(..))

data Bias
  = Dolorous
  | Skeptical
  | Impatient
  | Revelative
  | Trepidative
  | Repulsive
  | Desperative
  | Disapprobative

  | Contemptive
  | Exasperative
  | Indignative
  | Dismissive
  | Derisive
  | Pessimistic
  | Dubiative
  | Invidious
  | Disconcertive
  | Stupefactive
  | Fascinative
  | Infatuative
  | Euphoric

  | Delectative
  | Attentive
  | Renunciative
  | Mandatory
  | Exigent
  | Insipid
  | Admissive
  | Apprehensive

  | Prosaic
  | Comedic
  | Propositive
  | Suggestive
  | Diffident
  | Selective
  | Euphemistic
  | Corrective

  | Approbative
  | Ironic
  | Presumptive
  | Gratificative
  | Satiative
  | Perplexive
  | Contemplative
  | Propitious
  | Solicitative
  | Reactive
  | Coincidental
  | Fortuitous
  | Annunciative

  | Optimal
  | Assertive
  | Implicative
  | Accidental
  | Anticipative
  | Archetypal
  | Vexative
  | Dejective
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Bias where
  toAbbr = Abbr . \case
    Dolorous       -> "DOL"
    Skeptical      -> "SKP"
    Impatient      -> "IPT"
    Revelative     -> "RVL"
    Trepidative    -> "TRP"
    Repulsive      -> "RPU"
    Desperative    -> "DES"
    Disapprobative -> "DPB"

    Contemptive   -> "CTP"
    Exasperative  -> "EXA"
    Indignative   -> "IDG"
    Dismissive    -> "DIS"
    Derisive      -> "DRS"
    Pessimistic   -> "PES" -- BUG: PSM clashes with Presumptive
    Dubiative     -> "DUB"
    Invidious     -> "IVD"
    Disconcertive -> "DCC"
    Stupefactive  -> "STU"
    Fascinative   -> "FSC"
    Infatuative   -> "IFT"
    Euphoric      -> "EUH"

    Delectative  -> "DLC"
    Attentive    -> "ATE"
    Renunciative -> "RNC"
    Mandatory    -> "MND"
    Exigent      -> "EXG"
    Insipid      -> "ISP"
    Admissive    -> "ADM"
    Apprehensive -> "APH"

    Prosaic     -> "PSC"
    Comedic     -> "CMD"
    Propositive -> "PPV"
    Suggestive  -> "SGS"
    Diffident   -> "DFD"
    Selective   -> "SEL"
    Euphemistic -> "EUP"
    Corrective  -> "CRR"

    Approbative   -> "APB"
    Ironic        -> "IRO"
    Presumptive   -> "PSM"
    Gratificative -> "GRT"
    Satiative     -> "SAT"
    Perplexive    -> "PPX"
    Contemplative -> "CTV"
    Propitious    -> "PPT"
    Solicitative  -> "SOL"
    Reactive      -> "RAC"
    Coincidental  -> "COI"
    Fortuitous    -> "FOR"
    Annunciative  -> "ANN"

    Optimal      -> "OPT"
    Assertive    -> "ASV"
    Implicative  -> "IPL"
    Accidental   -> "ACC"
    Anticipative -> "ANP"
    Archetypal   -> "ACH"
    Vexative     -> "VEX"
    Dejective    -> "DEJ"

  fromAbbr (Abbr a) = case a of
    "DOL" -> Just Dolorous
    "SKP" -> Just Skeptical
    "IPT" -> Just Impatient
    "RVL" -> Just Revelative
    "TRP" -> Just Trepidative
    "RPU" -> Just Repulsive
    "DES" -> Just Desperative
    "DPB" -> Just Disapprobative

    "CTP" -> Just Contemptive
    "EXA" -> Just Exasperative
    "IDG" -> Just Indignative
    "DIS" -> Just Dismissive
    "DRS" -> Just Derisive
    "PES" -> Just Pessimistic -- BUG: PSM clashes with Presumptive
    "DUB" -> Just Dubiative
    "IVD" -> Just Invidious
    "DCC" -> Just Disconcertive
    "STU" -> Just Stupefactive
    "FSC" -> Just Fascinative
    "IFT" -> Just Infatuative
    "EUH" -> Just Euphoric

    "DLC" -> Just Delectative
    "ATE" -> Just Attentive
    "RNC" -> Just Renunciative
    "MND" -> Just Mandatory
    "EXG" -> Just Exigent
    "ISP" -> Just Insipid
    "ADM" -> Just Admissive
    "APH" -> Just Apprehensive

    "PSC" -> Just Prosaic
    "CMD" -> Just Comedic
    "PPV" -> Just Propositive
    "SGS" -> Just Suggestive
    "DFD" -> Just Diffident
    "SEL" -> Just Selective
    "EUP" -> Just Euphemistic
    "CRR" -> Just Corrective

    "APB" -> Just Approbative
    "IRO" -> Just Ironic
    "PSM" -> Just Presumptive
    "GRT" -> Just Gratificative
    "SAT" -> Just Satiative
    "PPX" -> Just Perplexive
    "CTV" -> Just Contemplative
    "PPT" -> Just Propitious
    "SOL" -> Just Solicitative
    "RAC" -> Just Reactive
    "COI" -> Just Coincidental
    "FOR" -> Just Fortuitous
    "ANN" -> Just Annunciative

    "OPT" -> Just Optimal
    "ASV" -> Just Assertive
    "IPL" -> Just Implicative
    "ACC" -> Just Accidental
    "ANP" -> Just Anticipative
    "ACH" -> Just Archetypal
    "VEX" -> Just Vexative
    "DEJ" -> Just Dejective
    _     -> Nothing
