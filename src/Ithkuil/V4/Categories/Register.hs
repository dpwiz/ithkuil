module Ithkuil.V4.Categories.Register
  ( Register(..),
  )
where

import Language.Class.Abbr (Abbreviated(..), Abbr(..))

data Register
  = Discursive
  | Parenthetical
  | Cogitant
  | Exemplificative
  | Specificative
  | Mathematical
  | CarrierEnd
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Register where
  toAbbr = Abbr . \case
    Discursive      -> "DSV"
    Parenthetical   -> "PNT"
    Cogitant        -> "COG"
    Exemplificative -> "EXM"
    Specificative   -> "SPF"
    Mathematical    -> "MTH"
    CarrierEnd      -> "CAR"

  fromAbbr (Abbr a) = case a of
    "DSV" -> Just Discursive
    "PNT" -> Just Parenthetical
    "COG" -> Just Cogitant
    "EXM" -> Just Exemplificative
    "SPF" -> Just Specificative
    "MTH" -> Just Mathematical
    "CAR" -> Just CarrierEnd
    _     -> Nothing
