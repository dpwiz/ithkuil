module Ithkuil.V4.Categories.Specification
  ( Specification (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Specification
  = Basic
  | Contential
  | Constitutive
  | Objective
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Specification where
  toAbbr = Abbr . \case
    Basic        -> "BSC"
    Contential   -> "CTE"
    Constitutive -> "CSV"
    Objective    -> "OBJ"

  fromAbbr (Abbr a) = case a of
    "BSC" -> Just Basic
    "CTE" -> Just Contential
    "CSV" -> Just Constitutive
    "OBJ" -> Just Objective
    _     -> Nothing
