module Ithkuil.V4.Categories.Affix
  ( Affix(..),
    AffixDegree(..),
    AffixType(..),
    AffixDelineation(..),
  )
where

data Affix
  = Affix
    { _affix       :: String,
      _degree      :: Maybe AffixDegree,
      _type        :: AffixType,
      _delineation :: AffixDelineation
    } deriving (Eq, Ord, Show)

data AffixDegree
  = AD1
  | AD2
  | AD3
  | AD4
  | AD5
  | AD6
  | AD7
  | AD8
  | AD9
  deriving (Eq, Ord, Enum, Bounded, Show)

data AffixType
  = -- | Circumstantial
    AT1
  | -- | Derivational
    AT2
    -- | Previous-only
  | AT3
  deriving (Eq, Ord, Enum, Bounded, Show)

data AffixDelineation
  = MainStem
  | IncStem
  deriving (Eq, Ord, Enum, Bounded, Show)
