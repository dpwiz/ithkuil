module Ithkuil.V4.Categories.Case
  ( Case(..),
    CaseGroup(..),
    caseGroups,
  ) where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Case
  = Thematic
  | Instrumental
  | Absolutive
  | Stimulative
  | Affective
  | Effectuative
  | Ergative
  | Dative
  | Inducive

  | Possessive
  | Proprietive
  | Genitive
  | Attributive
  | Productive
  | Interpretive
  | Originative
  | Interdependent
  | Partitive

  | Applicative
  | Purposive
  | Transmissive
  | Deferential
  | Contrastive
  | Transpositive
  | Commutative
  | Comparative
  | Considerative

  | Concessive
  | Aversive
  | Conversive
  | Situative
  | Functive
  | Transformative
  | Classificative
  | Consumptive
  | Resultative

  | Locative
  | Attendant
  | Allative
  | Ablative
  | Orientative
  | Interrelative
  | Intrative
  | Navigative

  | Assessive
  | Concursive
  | Periodic
  | Prolapsive
  | Precursive
  | Postcursive
  | Elapsive
  | Prolimitive

  | Referential
  | Correlative
  | Compositive
  | Dependent
  | Predicative
  | Essive
  | Assimilative
  | Conformative

  | Activative
  | Selective
  | Comitative
  | Utilitative
  | Descriptive
  | Relative
  | Terminative
  | Vocative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Case where
  toAbbr = Abbr . \case
    Thematic     -> "THM"
    Instrumental -> "INS"
    Absolutive   -> "ABS"
    Stimulative  -> "STM"
    Affective    -> "AFF"
    Effectuative -> "EFF"
    Ergative     -> "ERG"
    Dative       -> "DAT"
    Inducive     -> "IND"

    Possessive     -> "POS"
    Proprietive    -> "PRP"
    Genitive       -> "GEN"
    Attributive    -> "ATT"
    Productive     -> "PDC"
    Interpretive   -> "ITP"
    Originative    -> "OGN"
    Interdependent -> "IDP"
    Partitive      -> "PAR"

    Applicative   -> "APL"
    Purposive     -> "PUR"
    Transmissive  -> "TRA"
    Deferential   -> "DFR"
    Contrastive   -> "CRS"
    Transpositive -> "TSP"
    Commutative   -> "CMM"
    Comparative   -> "CMP"
    Considerative -> "CSD"

    Concessive     -> "CON"
    Aversive       -> "AVR"
    Conversive     -> "CVS"
    Situative      -> "SIT"
    Functive       -> "FUN"
    Transformative -> "TFM"
    Classificative -> "CLA"
    Consumptive    -> "CSM"
    Resultative    -> "RSL"

    Locative      -> "LOC"
    Attendant     -> "ATD"
    Allative      -> "ALL"
    Ablative      -> "ABL"
    Orientative   -> "ORI"
    Interrelative -> "IRL"
    Intrative     -> "INV"
    Navigative    -> "NAV"

    Assessive   -> "ASS"
    Concursive  -> "CNR"
    Periodic    -> "PER"
    Prolapsive  -> "PRO"
    Precursive  -> "PCV"
    Postcursive -> "PCR"
    Elapsive    -> "ELP"
    Prolimitive -> "PLM"

    Referential  -> "REF"
    Correlative  -> "COR"
    Compositive  -> "CPS"
    Dependent    -> "DEP"
    Predicative  -> "PRD"
    Essive       -> "ESS"
    Assimilative -> "ASI"
    Conformative -> "CFM"

    Activative  -> "ACT"
    Selective   -> "SEL"
    Comitative  -> "COM"
    Utilitative -> "UTL"
    Descriptive -> "DSC"
    Relative    -> "RLT"
    Terminative -> "TRM"
    Vocative    -> "VOC"

  fromAbbr (Abbr a) = case a of
    "THM" -> Just Thematic
    "INS" -> Just Instrumental
    "ABS" -> Just Absolutive
    "STM" -> Just Stimulative
    "AFF" -> Just Affective
    "EFF" -> Just Effectuative
    "ERG" -> Just Ergative
    "DAT" -> Just Dative
    "IND" -> Just Inducive

    "POS" -> Just Possessive
    "PRP" -> Just Proprietive
    "GEN" -> Just Genitive
    "ATT" -> Just Attributive
    "PDC" -> Just Productive
    "ITP" -> Just Interpretive
    "OGN" -> Just Originative
    "IDP" -> Just Interdependent
    "PAR" -> Just Partitive

    "APL" -> Just Applicative
    "PUR" -> Just Purposive
    "TRA" -> Just Transmissive
    "DFR" -> Just Deferential
    "CRS" -> Just Contrastive
    "TSP" -> Just Transpositive
    "CMM" -> Just Commutative
    "CMP" -> Just Comparative
    "CSD" -> Just Considerative

    "CON" -> Just Concessive
    "AVR" -> Just Aversive
    "CVS" -> Just Conversive
    "SIT" -> Just Situative
    "FUN" -> Just Functive
    "TFM" -> Just Transformative
    "CLA" -> Just Classificative
    "CSM" -> Just Consumptive
    "RSL" -> Just Resultative

    "LOC" -> Just Locative
    "ATD" -> Just Attendant
    "ALL" -> Just Allative
    "ABL" -> Just Ablative
    "ORI" -> Just Orientative
    "IRL" -> Just Interrelative
    "INV" -> Just Intrative
    "NAV" -> Just Navigative

    "ASS" -> Just Assessive
    "CNR" -> Just Concursive
    "PER" -> Just Periodic
    "PRO" -> Just Prolapsive
    "PCV" -> Just Precursive
    "PCR" -> Just Postcursive
    "ELP" -> Just Elapsive
    "PLM" -> Just Prolimitive

    "REF" -> Just Referential
    "COR" -> Just Correlative
    "CPS" -> Just Compositive
    "DEP" -> Just Dependent
    "PRD" -> Just Predicative
    "ESS" -> Just Essive
    "ASI" -> Just Assimilative
    "CFM" -> Just Conformative

    "ACT" -> Just Activative
    "SEL" -> Just Selective
    "COM" -> Just Comitative
    "UTL" -> Just Utilitative
    "DSC" -> Just Descriptive
    "RLT" -> Just Relative
    "TRM" -> Just Terminative
    "VOC" -> Just Vocative

    _     -> Nothing


data CaseGroup
  = Transrelative
  | Appositive
  | Associative
  | Adverbial
  | SpatioTemporal -- 1/2
  | Relational -- 1/2
  deriving (Eq, Ord, Enum, Bounded, Show)

caseGroups :: [(Case, CaseGroup)]
caseGroups =
  [ (Thematic, Transrelative)
  , (Instrumental, Transrelative)
  , (Absolutive, Transrelative)
  , (Stimulative, Transrelative)
  , (Affective, Transrelative)
  , (Effectuative, Transrelative)
  , (Ergative, Transrelative)
  , (Dative, Transrelative)
  , (Inducive, Transrelative)

  , (Possessive, Appositive)
  , (Proprietive, Appositive)
  , (Genitive, Appositive)
  , (Attributive, Appositive)
  , (Productive, Appositive)
  , (Interpretive, Appositive)
  , (Originative, Appositive)
  , (Interdependent, Appositive)
  , (Partitive, Appositive)

  , (Applicative, Associative)
  , (Purposive, Associative)
  , (Transmissive, Associative)
  , (Deferential, Associative)
  , (Contrastive, Associative)
  , (Transpositive, Associative)
  , (Commutative, Associative)
  , (Comparative, Associative)
  , (Considerative, Associative)

  , (Concessive, Adverbial)
  , (Aversive, Adverbial)
  , (Conversive, Adverbial)
  , (Situative, Adverbial)
  , (Functive, Adverbial)
  , (Transformative, Adverbial)
  , (Classificative, Adverbial)
  , (Consumptive, Adverbial)
  , (Resultative, Adverbial)

  , (Locative, SpatioTemporal)
  , (Attendant, SpatioTemporal)
  , (Allative, SpatioTemporal)
  , (Ablative, SpatioTemporal)
  , (Orientative, SpatioTemporal)
  , (Interrelative, SpatioTemporal)
  , (Intrative, SpatioTemporal)
  , (Navigative, SpatioTemporal)

  , (Assessive, SpatioTemporal)
  , (Concursive, SpatioTemporal)
  , (Periodic, SpatioTemporal)
  , (Prolapsive, SpatioTemporal)
  , (Precursive, SpatioTemporal)
  , (Postcursive, SpatioTemporal)
  , (Elapsive, SpatioTemporal)
  , (Prolimitive, SpatioTemporal)

  , (Referential, Relational)
  , (Correlative, Relational)
  , (Compositive, Relational)
  , (Dependent, Relational)
  , (Predicative, Relational)
  , (Essive, Relational)
  , (Assimilative, Relational)
  , (Conformative, Relational)

  , (Activative, Relational)
  , (Selective, Relational)
  , (Comitative, Relational)
  , (Utilitative, Relational)
  , (Descriptive, Relational)
  , (Relative, Relational)
  , (Terminative, Relational)
  , (Vocative, Relational)
  ]
