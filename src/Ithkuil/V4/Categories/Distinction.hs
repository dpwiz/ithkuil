module Ithkuil.V4.Categories.Distinction
  ( Distinction(..)
  ) where

data Distinction
  = Nominal
  | Verbal
  | Framed
  deriving (Eq, Ord, Enum, Bounded, Show)
