module Ithkuil.V4.Categories.Mood
  ( Mood (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Mood
  = Factual
  | Subjuntive
  | Assumptive
  | Speculative
  | Counterfactive
  | Hypotetical
  | Implicative
  | Ascriptive
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Mood where
  toAbbr = Abbr . \case
    Factual        -> "MNO"
    Subjuntive     -> "PRL"
    Assumptive     -> "CRO"
    Speculative    -> "RCP"
    Counterfactive -> "CPL"
    Hypotetical    -> "DUP"
    Implicative    -> "DEM"
    Ascriptive     -> "CNG"

  fromAbbr (Abbr a) = case a of
    "MNO" -> Just Factual
    "PRL" -> Just Subjuntive
    "CRO" -> Just Assumptive
    "RCP" -> Just Speculative
    "CPL" -> Just Counterfactive
    "DUP" -> Just Hypotetical
    "DEM" -> Just Implicative
    "CNG" -> Just Ascriptive
    _     -> Nothing
