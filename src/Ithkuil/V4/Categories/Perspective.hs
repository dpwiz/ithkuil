module Ithkuil.V4.Categories.Perspective
  ( Perspective (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Perspective
  = Monadic
  | Polyadic
  | Nomic
  | Abstract
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Perspective where
  toAbbr = Abbr . \case
    Monadic  -> "M"
    Polyadic -> "N"
    Nomic    -> "P"
    Abstract -> "A"

  fromAbbr (Abbr a) = case a of
    "M" -> Just Monadic
    "N" -> Just Polyadic
    "P" -> Just Nomic
    "A" -> Just Abstract
    _   -> Nothing
