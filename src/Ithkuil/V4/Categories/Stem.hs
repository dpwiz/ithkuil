module Ithkuil.V4.Categories.Stem
  ( Stem(..),
  )
where

data Stem
  = Stem1
  | Stem2
  | Stem3
  | Stem0
  deriving (Eq, Ord, Enum, Bounded, Show)
