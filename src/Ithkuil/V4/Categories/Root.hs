module Ithkuil.V4.Categories.Root
  ( Root(..),
  )
where

data Root
  = Root String
  deriving (Eq, Ord, Show)
