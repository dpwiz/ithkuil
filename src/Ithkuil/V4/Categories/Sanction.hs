module Ithkuil.V4.Categories.Sanction
  ( Sanction (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Sanction
  = PPS
  | EPI
  | ALG
  | IPU
  | RFU
  | REB
  | CJT
  | EXV
  | AXM
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Sanction where
  toAbbr = Abbr . \case
    PPS -> "PPS"
    EPI -> "EPI"
    ALG -> "ALG"
    IPU -> "IPU"
    RFU -> "RFU"
    REB -> "REB"
    CJT -> "CJT"
    EXV -> "EXV"
    AXM -> "AXM"

  fromAbbr (Abbr a) = case a of
    "PPS" -> Just PPS
    "EPI" -> Just EPI
    "ALG" -> Just ALG
    "IPU" -> Just IPU
    "RFU" -> Just RFU
    "REB" -> Just REB
    "CJT" -> Just CJT
    "EXV" -> Just EXV
    "AXM" -> Just AXM
    _     -> Nothing
