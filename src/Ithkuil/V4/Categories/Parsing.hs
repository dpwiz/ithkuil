module Ithkuil.V4.Categories.Parsing
  ( Parsing(..),
  )
where

data Parsing
  = FollowMono
  | WordUltimate
  | FollowPen
  | FollowAntepen
  | FollowPreantepen
  deriving (Eq, Ord, Enum, Bounded, Show)
