module Ithkuil.V4.Categories.IncType
  ( IncType(..)
  ) where

data IncType
  = Circumstantial
  | Derivational
  deriving (Eq, Ord, Enum, Bounded, Show)
