module Ithkuil.V4.Categories.Context
  ( Context (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Context
  = Existential
  | Functional
  | Representative
  | Amalgamative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Context where
  toAbbr = Abbr . \case
    Existential    -> "EXS"
    Functional     -> "FNC"
    Representative -> "RPV"
    Amalgamative   -> "AMG"

  fromAbbr (Abbr a) = case a of
    "EXS" -> Just Existential
    "FNC" -> Just Functional
    "RPV" -> Just Representative
    "AMG" -> Just Amalgamative
    _     -> Nothing
