module Ithkuil.V4.Categories.Designation
  ( Designation(..)
  ) where

data Designation
  = Informal
  | Formal
  deriving (Eq, Ord, Enum, Bounded, Show)
