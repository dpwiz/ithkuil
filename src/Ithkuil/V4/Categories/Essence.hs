module Ithkuil.V4.Categories.Essence
  ( Essence (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Essence
  = Normal
  | Representative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Essence where
  toAbbr = Abbr . \case
    Normal         -> "NRM"
    Representative -> "RPV"

  fromAbbr (Abbr a) = case a of
    "NRM" -> Just Normal
    "RPV" -> Just Representative
    _     -> Nothing
