module Ithkuil.V4.Categories.Aspect
  ( Aspect (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Aspect
  = Retrospective
  | Prospective
  | Habitual
  | Progressive
  | Imminent
  | Precessive
  | Regulative
  | Antecedent
  | Anticipatory

  | Resumptive
  | Cessative
  | Pausal
  | Regressive
  | Preclusive
  | Continuative
  | Incessative
  | Summative
  | Interruptive

  | Preemptive
  | Climactic
  | Dilatory
  | Temporary
  | Motive
  | Sequential
  | Expeditive
  | Protractive
  | Preparatory

  | Disclusive
  | Conclusive
  | Culminative
  | Intermediative
  | Tardative
  | Transitional
  | Intercommutative
  | Expenditive
  | Limitative
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Aspect where
  toAbbr = Abbr . \case
    Retrospective -> "RTR"
    Prospective   -> "PRS"
    Habitual      -> "HAB"
    Progressive   -> "PRG"
    Imminent      -> "IMM"
    Precessive    -> "PCS"
    Regulative    -> "REG"
    Antecedent    -> "ATC"
    Anticipatory  -> "ATP"

    Resumptive   -> "RSM"
    Cessative    -> "CSS"
    Pausal       -> "PAU"
    Regressive   -> "RGR"
    Preclusive   -> "PCL"
    Continuative -> "CNT"
    Incessative  -> "ICS"
    Summative    -> "SMM"
    Interruptive -> "IRP"

    Preemptive  -> "PMP"
    Climactic   -> "CLM"
    Dilatory    -> "DLT"
    Temporary   -> "TMP"
    Motive      -> "MTV"
    Sequential  -> "SQN"
    Expeditive  -> "EPD"
    Protractive -> "PTC"
    Preparatory -> "PPR"

    Disclusive       -> "DCL"
    Conclusive       -> "CCL"
    Culminative      -> "CUL"
    Intermediative   -> "IMD"
    Tardative        -> "TRD"
    Transitional     -> "TNS"
    Intercommutative -> "ITC"
    Expenditive      -> "XPD"
    Limitative       -> "LIM"

  fromAbbr (Abbr a) = case a of
    "RTR" -> Just Retrospective
    "PRS" -> Just Prospective
    "HAB" -> Just Habitual
    "PRG" -> Just Progressive
    "IMM" -> Just Imminent
    "PCS" -> Just Precessive
    "REG" -> Just Regulative
    "ATC" -> Just Antecedent
    "ATP" -> Just Anticipatory

    "RSM" -> Just Resumptive
    "CSS" -> Just Cessative
    "PAU" -> Just Pausal
    "RGR" -> Just Regressive
    "PCL" -> Just Preclusive
    "CNT" -> Just Continuative
    "ICS" -> Just Incessative
    "SMM" -> Just Summative
    "IRP" -> Just Interruptive

    "PMP" -> Just Preemptive
    "CLM" -> Just Climactic
    "DLT" -> Just Dilatory
    "TMP" -> Just Temporary
    "MTV" -> Just Motive
    "SQN" -> Just Sequential
    "EPD" -> Just Expeditive
    "PTC" -> Just Protractive
    "PPR" -> Just Preparatory

    "DCL" -> Just Disclusive
    "CCL" -> Just Conclusive
    "CUL" -> Just Culminative
    "IMD" -> Just Intermediative
    "TRD" -> Just Tardative
    "TNS" -> Just Transitional
    "ITC" -> Just Intercommutative
    "XPD" -> Just Expenditive
    "LIM" -> Just Limitative

    _     -> Nothing
