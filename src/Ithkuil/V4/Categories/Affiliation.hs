module Ithkuil.V4.Categories.Affiliation
  ( Affiliation (..),
  )
where

import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Affiliation
  = Consolidative
  | Associative
  | Variative
  | Coalescent
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Abbreviated Affiliation where
  toAbbr = Abbr . \case
    Consolidative -> "CSL"
    Associative   -> "ASO"
    Variative     -> "VAR"
    Coalescent    -> "COA"

  fromAbbr (Abbr a) = case a of
    "CSL" -> Just Consolidative
    "ASO" -> Just Associative
    "VAR" -> Just Variative
    "COA" -> Just Coalescent
    _     -> Nothing
