module Ithkuil.V4.Categories.Configuration
  ( Configuration (..),
    Potential(..),
    Connectedness(..),
    Similarity(..),
  )
where

-- import Language.Class.Abbr (Abbr (..), Abbreviated (..))

data Configuration
  = Uniplex Potential
  | Duplex Connectedness Similarity
  | Multiplex Connectedness Similarity
  deriving (Eq, Ord, Show)

data Potential
  = Specific
  | Potential
  deriving (Eq, Ord, Enum, Bounded, Show)

data Connectedness
  = Separate
  | Connected
  | Fused
  deriving (Eq, Ord, Enum, Bounded, Show)

data Similarity
  = Similar
  | Dissimilar
  | Fuzzy
  deriving (Eq, Ord, Enum, Bounded, Show)

-- instance Abbreviated Configuration where
--   toAbbr = Abbr . \case
--     Uniplex pot       -> toAbbr pot
--     Duplex con sim    -> toAbbr con <> "/" <> toAbbr sim
--     Multiplex con sim -> toAbbr con <> "/" <> toAbbr sim

--   fromAbbr (Abbr a) = case a of
