module Language.Class.Abbr
  ( Abbreviated(..),
    Abbr(..),
  )
where

newtype Abbr = Abbr String
  deriving (Eq, Ord, Show)

class Abbreviated a where

  -- | Pack value into short string.
  toAbbr :: a -> Abbr

  -- | Restore value.
  fromAbbr :: Abbr -> Maybe a
